import 'package:auth_bloc_with_freezed/presentation/auth/presentation/widget/phone_verification.dart';
import 'package:flutter/material.dart';

class SignInWithNumberPage extends StatefulWidget {
  const SignInWithNumberPage({Key? key}) : super(key: key);

  @override
  State<SignInWithNumberPage> createState() => _SignInWithNumberPageState();
}

class _SignInWithNumberPageState extends State<SignInWithNumberPage> {
  final GlobalKey<FormState> _formKey = GlobalKey();
  late TextEditingController phoneTextController;
  late TextEditingController codeTextController;

  @override
  void initState() {
    phoneTextController = TextEditingController();
    codeTextController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    phoneTextController.dispose();
    codeTextController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text('Sign In with Number'),
      ),
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            PhoneVerification(
              phoneTextController,
              codeTextController,
              authType: AuthType.signIn,
            ),
          ],
        ),
      ),
    );
  }
}
