import 'package:auth_bloc_with_freezed/core/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_multi_formatter/flutter_multi_formatter.dart';

enum AuthType { signUp, signIn }

class PhoneVerification extends StatelessWidget {
  final TextEditingController phoneController;
  final TextEditingController codeController;
  final AuthType authType;

  const PhoneVerification(
    this.codeController,
    this.phoneController, {
    Key? key,
    required this.authType,
  }) : super(key: key);

  String get phoneNumberHint => '00 000 00 00';

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TextFormField(
            keyboardType: TextInputType.number,
            controller: phoneController,
            inputFormatters: [MaskedInputFormatter(phoneNumberHint)],
            decoration: InputDecoration(
              hintText: 'phoneNumber',
              hintStyle: kRegularBlack14.copyWith(color: kGrey),
              prefixIcon: TextButton(
                onPressed: () {},
                child: const Text(
                  '+998',
                  style: kMediumBlack16,
                ),
              ),
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          ElevatedButton(
            onPressed: () {},
            child: const Text('Get the sms'),
          ),
          const SizedBox(
            height: 10,
          ),
          TextFormField(
            maxLines: 1,
            controller: codeController,
            keyboardType: TextInputType.number,
            validator: (value) {
              if (value != null && value.isEmpty) {
                return 'not empty';
              }
            },
            decoration: InputDecoration(
              hintText: 'sms code',
              hintStyle: kRegularBlack14.copyWith(color: kGrey),
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          SizedBox(
            width: double.infinity,
            child: ElevatedButton(
              onPressed: () {},
              child: const Text('SignIn'),
            ),
          ),
        ],
      ),
    );
  }
}
