import 'package:auth_bloc_with_freezed/core/error_pages.dart';
import 'package:auth_bloc_with_freezed/presentation/auth/presentation/sign_in/sign_in_with_number_page.dart';
import 'package:flutter/material.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      onUnknownRoute: (RouteSettings settings) {
        return MaterialPageRoute<void>(
          settings: settings,
          builder: (BuildContext context) => const PageNotFound(),
        );
      },
      home: SignInWithNumberPage(),
    );
  }
}
