import 'package:auth_bloc_with_freezed/core/style.dart';
import 'package:auth_bloc_with_freezed/core/ui_helpers.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

class RetryButton extends StatelessWidget {
  final VoidCallback onTap;

  const RetryButton({required this.onTap});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 17.0, vertical: 13.0),
      decoration: BoxDecoration(
        gradient: kGradientHorizontal,
        borderRadius: BorderRadius.circular(13.0),
      ),
      child: InkWell(
        borderRadius: const BorderRadius.all(Radius.circular(13.0)),
        onTap: onTap,
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Text(
              "refresh".tr(),
              style: kRegularBlack14.copyWith(
                  color: Colors.white, letterSpacing: 0.1),
            ),
            horizontalSpace10,
            const Icon(
              Icons.refresh,
              color: Colors.white,
              size: 18,
            )
          ],
        ),
      ),
    );
  }
}
