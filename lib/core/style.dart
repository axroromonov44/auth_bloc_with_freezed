import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

const Color kBlack = Color.fromRGBO(51, 51, 51, 1);
const Color kGrey = Color.fromRGBO(130, 130, 130, 1);
const Color kBlue = Color.fromRGBO(45, 156, 219, 1);
const Color kWhite = Color.fromRGBO(255, 255, 255, 1);
const Color kLightGrey = Color.fromRGBO(237, 237, 237, 1);
const Color kBackgroundColor = Color.fromRGBO(242, 242, 242, 1);
const Color kPrimaryColor = Color.fromRGBO(251, 111, 78, 1);
const Color kYellow = Color.fromRGBO(251, 218, 109, 1);
const Color kRed = Color.fromRGBO(254, 76, 76, 1);
const Color kLightOrange = Color.fromRGBO(255, 235, 229, 1);
const Color kGreen = Color.fromRGBO(41, 241, 195, 1);

const kSemiBoldBlack24 =
TextStyle(fontWeight: FontWeight.w600, fontSize: 24.0, color: kBlack);
const kSemiBoldBlack20 =
TextStyle(fontWeight: FontWeight.w600, fontSize: 20.0, color: kBlack);
const kSemiBoldBlack16 =
TextStyle(fontWeight: FontWeight.w600, fontSize: 16.0, color: kBlack);
const kSemiBoldBlack14 =
TextStyle(fontWeight: FontWeight.w600, fontSize: 14.0, color: kBlack);
TextStyle kSemiBoldBlack12 =
TextStyle(fontWeight: FontWeight.w600, fontSize: 12.0, color: kBlack);

const kMediumBlack16 =
TextStyle(fontWeight: FontWeight.w500, fontSize: 16.0, color: kBlack);
const kMediumBlack14 =
TextStyle(fontWeight: FontWeight.w500, fontSize: 14.0, color: kBlack);
const kMediumBlack12 =
TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0, color: kBlack);
const kMediumBlack10 =
TextStyle(fontWeight: FontWeight.w500, fontSize: 10.0, color: kBlack);

const kRegularBlack16 =
TextStyle(fontWeight: FontWeight.w400, fontSize: 16.0, color: kBlack);
const kRegularBlack14 =
TextStyle(fontWeight: FontWeight.w400, fontSize: 14.0, color: kBlack);
TextStyle kRegularBlack12 =
TextStyle(fontWeight: FontWeight.w400, fontSize: 12.0, color: kBlack);
const kRegularBlack10 =
TextStyle(fontWeight: FontWeight.w400, fontSize: 10.0, color: kBlack);
const kRegularBlack9 =
TextStyle(fontWeight: FontWeight.w400, fontSize: 9.0, color: kBlack);

const kRegularGrey14 =
TextStyle(fontWeight: FontWeight.w400, fontSize: 14.0, color: kGrey);
const kRegularGrey12 =
TextStyle(fontWeight: FontWeight.w400, fontSize: 12.0, color: kGrey);
const kRegularGrey10 =
TextStyle(fontWeight: FontWeight.w400, fontSize: 10.0, color: kGrey);

const kRegularBlue14 =
TextStyle(fontWeight: FontWeight.w400, fontSize: 14.0, color: kBlue);

const Gradient kGradientHorizontal = LinearGradient(
  colors: [
    Color.fromRGBO(238, 146, 54, 1),
    Color.fromRGBO(234, 95, 42, 1),
  ],
);
const Gradient kWhiteGradientHorizontal = LinearGradient(
  colors: [
    Color.fromRGBO(242, 242, 242, 1),
    Color.fromRGBO(242, 242, 242, 0.9),
  ],
);

const Gradient kGradientVertical = LinearGradient(
  begin: Alignment.topCenter,
  end: Alignment.bottomCenter,
  colors: [
    Color.fromRGBO(238, 146, 54, 1),
    Color.fromRGBO(234, 95, 42, 1),
  ],
);

const Gradient kGradientRightVertical = LinearGradient(
  begin: Alignment.topCenter,
  end: Alignment.bottomCenter,
  colors: [
    Color.fromRGBO(254, 76, 76, 1),
    Color.fromRGBO(254, 118, 0, 1),
  ],
);

const Gradient kYellowGradientLeftVertical = LinearGradient(
  begin: Alignment.topCenter,
  end: Alignment.bottomCenter,
  colors: [
    Color.fromRGBO(254, 150, 2, 1),
    Color.fromRGBO(254, 200, 0, 1),
  ],
);
